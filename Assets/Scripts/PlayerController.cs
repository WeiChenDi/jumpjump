﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    protected GameObject _body;
    protected GameObject _head;
    protected Rigidbody _rigid;
    protected MicrophoneTest _micro;
    protected int _jumpForce = 0;
    protected Stage _nextStageComponent;
    //protected GameObject _currentStage;
    //protected Collider _lastCollisionCollider;

    // Start is called before the first frame update
    void Start()
    {
        MicrophoneTest.Instance.IsOver = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void OnGetScore(Stage currentStage)
    {
        currentStage.isNewStage = false;
        ScoreMgr.Instance.GetScore(currentStage);
    }

    protected void OnGetDoubleScore(Stage currentStage)
    {
        currentStage.isNewStage = false;
        ScoreMgr.Instance.GetDoubleScore(currentStage);
    }

    protected void StopSwing(Stage currentStage)
    {
        StageSwing stageSwingComponent = currentStage.gameObject.GetComponent<StageSwing>();
        stageSwingComponent.StopSwing();
    }

    protected void OnGameOver()
    {
        MicrophoneTest.Instance.IsOver = true;
        ScoreMgr.Instance.GameOver();
    }
}
