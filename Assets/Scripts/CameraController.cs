﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class CameraController : MonoBehaviour
{

    private GameObject _player;
    private Vector3 _cameraRelativePosition;

    private static CameraController _instance;
    public static CameraController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.Find("Main Camera").GetComponent<CameraController>();
            }
            return _instance;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player");
        _cameraRelativePosition = transform.position - _player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MovingCamera()
    {
        transform.DOMove(_player.transform.position + _cameraRelativePosition, 1);
    }
}
