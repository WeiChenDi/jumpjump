﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{

    private float _speed = 2f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Rotate(new Vector3(0, 0, 1) * _speed);  //绕y轴旋转
    }

    private void OnTriggerEnter(Collider other)
    {
        gameObject.SetActive(false);
        ScoreMgr.Instance.GetCoinScore();
    }
}
