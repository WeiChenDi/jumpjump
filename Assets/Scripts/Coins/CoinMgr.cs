﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinMgr : MonoBehaviour
{
    private Vector3 _pos1;
    private Vector3 _pos2;
    public GameObject _player;
    public GameObject coinPre;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private static CoinMgr _instance;
    public static CoinMgr Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.Find("CoinMgr").GetComponent<CoinMgr>();
            }
            return _instance;
        }
    }

    public void CreateCoin(GameObject stage2)
    {
        _pos1 = _player.transform.position;
        _pos2 = stage2.transform.GetChild(0).transform.position;
        float distance;
        Vector3 coinPos;
        if (SpawnStage.Instance.Direction.z == 0f)
        {
            distance = _pos2.x - _pos1.x;
            float x = Random.Range(distance / 4, distance * 3 / 4);
            coinPos = new Vector3(x + _pos1.x, GetHeight(x, distance), _pos1.z);
        }
        else
        {
            distance = _pos2.z - _pos1.z;
            float z = Random.Range(distance / 4, distance * 3 / 4);
            coinPos = new Vector3(_pos1.x, GetHeight(z, distance),  z + _pos1.z);
        }
        GameObject coinObj = Instantiate(coinPre, transform, true);
        coinObj.transform.position = coinPos;
    }

    private float GetHeight(float x, float distance)
    {
        float result = 0f;
        result = (2 * Mathf.Sqrt(2) - 1) * (x - (1 / distance) * x * x);
        return result;
    }
}
