﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpawnStage : MonoBehaviour
{
    public float maxDistance = 3f;
    public float minDistance = 1.5f;
    private Vector3 _direction = new Vector3(1, 0, 0);
    public Vector3 Direction
    {
        get
        {
            return _direction;
        }
    }

    //private const float obstacleStageChance = 0.2f;
    //private const float movableStageChance = 0.2f;
    //private const float swingStageChance = 0.1f;
    private const float obstacleStageChance = 0.2f;
    private const float movableStageChance = 0.2f;
    private const float swingStageChance = 0.1f;
    private const float coinStageChhance = 0.2f;

    private const string stagePath = "Prefabs/Stages";
    private List<GameObject> _stagesObjs;

    private GameObject _stage;
    private GameObject _stageParent;

    private GameObject _currentStage;
    public GameObject CurrentStage
    {
        get
        {
            return _currentStage;
        }
        set
        {
            _currentStage = value;
        }
    }

    private Collider _lastCollisionCollider;
    public Collider LastCollisionCollider
    {
        get
        {
            return _lastCollisionCollider;
        }
        set
        {
            _lastCollisionCollider = value;
        }
    }

    private float _cubeScale;
    public float CubeScale
    {
        get
        {
            return _cubeScale;
        }
    }

    private static SpawnStage _instance;
    public static SpawnStage Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.Find("Player").GetComponent<SpawnStage>();
            }
            return _instance;
        }
    }
    
    // Start is called before the first frame update
    void Awake()
    {
        _stage = GameObject.Find("StartStage");
        _stageParent = GameObject.Find("Stages");
        _stagesObjs = new List<GameObject>();
        Object[] tmpObj = Resources.LoadAll(stagePath);
        for(int i = 0; i < tmpObj.Length; i++)
        {
            _stagesObjs.Add(tmpObj[i] as GameObject);
        }
        _currentStage = _stage;
        Instance._lastCollisionCollider = _currentStage.GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Stage CreateNewStage()
    {
        Stage newStageComponent;
        int stageIndex = (int)Random.Range(0f, _stagesObjs.Count);
        GameObject stageObj = Instantiate(_stagesObjs[stageIndex]);
        newStageComponent = stageObj.AddComponent<Stage>();
        newStageComponent.stageType = SpawnStageType();
        newStageComponent.direction = _direction;
        stageObj.transform.parent = _stageParent.transform;
        Vector3 curPos = new Vector3(transform.position.x, _currentStage.transform.position.y, transform.position.z);
        if (newStageComponent.stageType == StageType.ObstacleStage)
        {
            stageObj.transform.position = curPos + newStageComponent.direction * Random.Range(1f + 1f, maxDistance);
            newStageComponent.CreateObstacle();
        }
        else if(newStageComponent.stageType == StageType.MovableStage)
        {
            stageObj.tag = "Dragable";
            stageObj.transform.position = curPos + newStageComponent.direction * Random.Range(1f + 1f, maxDistance) + new Vector3(0, Random.Range(0.5f, 1.5f));
        }
        else if(newStageComponent.stageType == StageType.SwingStage)
        {
            stageObj.transform.position = curPos + newStageComponent.direction * Random.Range(1f + 1f, maxDistance);
            stageObj.AddComponent<StageSwing>();
        }
        else if(newStageComponent.stageType == StageType.CoinStage)
        {
            stageObj.transform.position = curPos + newStageComponent.direction * Random.Range(1f + 1f, maxDistance);
            CoinMgr.Instance.CreateCoin(newStageComponent.gameObject);
        }
        else
        {
            stageObj.transform.position = curPos + newStageComponent.direction * Random.Range(minDistance, maxDistance);
        }
        return newStageComponent;
    }

    private StageType SpawnStageType()
    {
        float tmp = Random.Range(0f, 1f);
        if(tmp >= 0 && tmp < obstacleStageChance)
        {
            return StageType.ObstacleStage;
        }
        else if(tmp < movableStageChance + obstacleStageChance)
        {
            // return StageType.MovableStage;
            return StageType.MovableStage;
        }
        else if(tmp < swingStageChance + movableStageChance + obstacleStageChance)
        {
            return StageType.SwingStage;
        }
        else if(tmp < swingStageChance + movableStageChance + obstacleStageChance + coinStageChhance)
        {
            return StageType.CoinStage;
        }
        else
        {
            return StageType.NormalStage;
        }
    }

    public void RandomDirection()
    {
        var seed = Random.Range(0, 2);
        if (seed == 0)
        {
            _direction = new Vector3(1, 0, 0);
        }
        else
        {
            _direction = new Vector3(0, 0, 1);
        }
    }
}
