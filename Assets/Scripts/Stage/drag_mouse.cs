﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drag_mouse : MonoBehaviour
{
    private Camera cam; // main camera
    private GameObject go;  // the selected object
    private bool isTouched = false;
    private bool isDrag = false;
    private Vector3 m_pos;
    private Vector3 initPos;   
    private Vector3 currentPos;
    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        m_pos = Input.mousePosition;
        Ray ray = cam.ScreenPointToRay(m_pos);
        RaycastHit hit;

        if(!isDrag)
        {
            if(Physics.Raycast(ray, out hit))
            {
                //Debug.DrawLine(ray.origin, hit.point);
                go = hit.collider.gameObject;   // get the selected object
                initPos = cam.WorldToScreenPoint(go.transform.position);
                offset = go.transform.position - cam.ScreenToWorldPoint(new Vector3(m_pos.x, m_pos.y, initPos.z));
                isTouched = true;
            }
        }

        if(Input.GetMouseButton(0))
        {
            Vector3 currentScreenSpace = new Vector3(m_pos.x, m_pos.y, initPos.z);

            currentPos = cam.ScreenToWorldPoint(currentScreenSpace) + offset;

            if(isTouched&&go.transform.CompareTag("Dragable"))
            {
                if(currentPos.y>=1.5)
                {
                    currentPos.y = 1.5f;
                }
                if(currentPos.y<=0)
                {
                    currentPos.y = 0;
                }
                go.transform.position = new Vector3(go.transform.position.x, currentPos.y, go.transform.position.z);
            }

            isDrag = true;
        }
        else
        {
            isDrag = false;
        }
    }
}
