﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drag_touch : MonoBehaviour
{
    private Camera cam;
    private GameObject go;
    private bool isTouched = false;
    private bool isDrag = false;
    private Vector3 touchPos;
    private Vector3 initPos;
    private Vector3 currentPos;
    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                touchPos = Input.GetTouch(0).position;
                Ray ray = cam.ScreenPointToRay(touchPos);
                RaycastHit hit;

                if (!isDrag)
                {
                    if (Physics.Raycast(ray, out hit))
                    {
                        go = hit.collider.gameObject;
                        initPos = cam.WorldToScreenPoint(go.transform.position);
                        offset = go.transform.position - cam.ScreenToWorldPoint(new Vector3(touchPos.x, touchPos.y, initPos.z));
                        isTouched = true;
                    }
                }
            }

            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                Vector3 currentScreenSpace = new Vector3(touchPos.x, touchPos.y, initPos.z);

                currentPos = cam.ScreenToWorldPoint(currentScreenSpace) + offset;

                if (isTouched && go.transform.CompareTag("Dragable"))
                {
                    if (currentPos.y >= 1.5)
                    {
                        currentPos.y = 1.5f;
                    }
                    if (currentPos.y <= 0)
                    {
                        currentPos.y = 0;
                    }
                    go.transform.position = new Vector3(go.transform.position.x, currentPos.y, go.transform.position.z);
                }

                isDrag = true;
            }
            else
            {
                isDrag = false;
            }
        }
    }
}
