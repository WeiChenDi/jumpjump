﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CommonAPI;

public class CenterMarkMgr : MonoBehaviour
{

    private GameObject _player;
    private Collider _playerCollider;
    private GameObject _haloObj;
    private Animator _animator;

    public bool collided;


    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player");
        _playerCollider = _player.transform.Find("Body").GetComponent<BoxCollider>();
        _haloObj = transform.parent.Find("Halo").gameObject;
        _animator = _haloObj.GetComponent<Animator>();
        //EventMgr.AddListener(EventTypeDef.OnCenterEnter, CenterEnter);
    }

    private void OnDestroy()
    {
        //EventMgr.RemoveListener(EventTypeDef.OnCenterEnter, CenterEnter);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other == _playerCollider)
        {
            collided = true;
            if (transform.parent.name != "StartStage")
            {
                _haloObj.SetActive(true);
                _animator.Play("Halo");
                //EventMgr.Dispatch(EventTypeDef.OnCenterEnter);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        collided = false;
    }

    //public void CenterEnter()
    //{

    //}
}
