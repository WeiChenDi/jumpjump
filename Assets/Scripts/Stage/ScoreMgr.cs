﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreMgr : MonoBehaviour
{

    private const int _normalScore = 50;
    private const int _touchBreakScore = 10;
    private const int _moveableStageScore = 60;
    private const int _swingStageScore = 100;
    private const int _coinScore = 30;

    private LeaderBoardScript leaderBoard;
    public Text scoreText;

    private static ScoreMgr _instance;
    public static ScoreMgr Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.Find("ScoreMgr").GetComponent<ScoreMgr>();
            }
            return _instance;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        leaderBoard = FindObjectOfType<LeaderBoardScript>();
        GameObject canvas = GameObject.Find("Canvas");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GameOver()
    {
        leaderBoard.OnGameOver();
    }

    public void GetScore(Stage currentStage)
    {
        if (currentStage.name != "StartStage")
        {
            if(currentStage.stageType == StageType.MovableStage)
            {
                AddScore(_moveableStageScore);
            }
            else if(currentStage.stageType == StageType.ObstacleStage)
            {
                AddScore(_normalScore);
            }
            else if(currentStage.stageType == StageType.NormalStage)
            {
                AddScore(_normalScore);
            }
            else if(currentStage.stageType == StageType.SwingStage)
            {
                AddScore(_swingStageScore);
            }
            else if(currentStage.stageType == StageType.CoinStage)
            {
                AddScore(_normalScore);
            }
        }
    }

    public void GetDoubleScore(Stage currentStage)
    {
        if (currentStage.name != "StartStage")
        {
            if (currentStage.stageType == StageType.MovableStage)
            {
                AddScore(_moveableStageScore * 2);
            }
            else if (currentStage.stageType == StageType.NormalStage)
            {
                AddScore(_normalScore * 2);
            }
            else if (currentStage.stageType == StageType.SwingStage)
            {
                AddScore(_swingStageScore * 2);
            }
        }
    }

    public void GetTouchBreakScore()
    {
        AddScore(_touchBreakScore);
    }

    public void GetCoinScore()
    {
        AddScore(_coinScore);
    }

    private void AddScore(int score)
    {
        leaderBoard.currentScore += score;
        scoreText.text = leaderBoard.currentScore.ToString();
    }
}
