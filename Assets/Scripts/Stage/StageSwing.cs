﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSwing : MonoBehaviour
{
    private float _distance;
    private const float _maxDistance = 1f;
    private float _speed = 0.0f;
    private const float _addSpeed = 0.1f;
    private const float _maxSpeed = 2f;
    private Vector3 _startPos;
    private Vector3 _dir1 = new Vector3(1, 0, 0);
    private Vector3 _dir2 = new Vector3(0, 1, 0);
    private bool _swinging = true;

    // Start is called before the first frame update
    void Start()
    {
        _startPos = transform.position;
        _speed = _maxSpeed;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(_swinging)
        {
            Swing();
        }
    }

    private void Swing()
    {
        if (_distance > 0f && _speed > 0f)
        {
            _speed -= _addSpeed;
        }
        else if (_distance > 0f && _speed < 0f)
        {
            _speed -= _addSpeed;
        }
        else if (_distance <= 0f && _speed < 0f)
        {
            _speed += _addSpeed;
        }
        else if (_distance <= 0f && _speed > 0f)
        {
            _speed += _addSpeed;
        }
        else if (_speed == 0f)
        {
            if (_distance > 0)
            {
                _speed -= _addSpeed;
            }
            if (_distance < 0)
            {
                _speed += _addSpeed;
            }
        }
        if (SpawnStage.Instance.Direction == _dir1)
        {
            transform.Translate(_dir2 * -1 * Time.deltaTime * _speed);
        }
        else
        {
            transform.Translate(_dir1 * Time.deltaTime * _speed);
        }
        Vector3 tmp = transform.position - _startPos;
        _distance = tmp.x + tmp.z;
    }

    public void StopSwing()
    {
        _swinging = false;
    }
}
