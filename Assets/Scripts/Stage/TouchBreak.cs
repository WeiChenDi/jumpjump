﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchBreak : MonoBehaviour
{
    private int _layerMask;
    private int _touchCount = 0;
    private bool _gotScore = false;
    //public GameObject stage_break;

    private bool ANDROID = false;
    private bool STANDALONE = false;

    void Awake()
    {
#if UNITY_ANDROID 
        ANDROID = true;
#endif

#if UNITY_STANDALONE
        STANDALONE = true;
#endif
    }

    // Start is called before the first frame update
    void Start()
    {
        _layerMask = 1 << 2;
        //_layerMask = ~_layerMask;

    }

    // Update is called once per frame
    void Update()
    {
        if (STANDALONE)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit _raycasthist;
                if (Physics.Raycast(_ray, out _raycasthist, 1000f, _layerMask))
                {
                    _touchCount++;
                }
            }
            if (_touchCount >= 5)
            {
                if(!_gotScore)
                {
                    ScoreMgr.Instance.GetTouchBreakScore();
                    _gotScore = true;
                }
                //Destroy(gameObject);
                DestroyWall();
            }
        }
        else if(ANDROID)
        {
            if (Input.touchCount >= 1)
            {
                Ray _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit _raycasthist;
                if (Physics.Raycast(_ray, out _raycasthist, 1000f, _layerMask))
                {
                    Debug.Log("Hit!");
                    _touchCount++;
                }
            }
            if (_touchCount >= 5)
            {
                if (!_gotScore)
                {
                    ScoreMgr.Instance.GetTouchBreakScore();
                    _gotScore = true;
                }
                DestroyWall();
            }
        }
    }

    private void DestroyWall()
    {
        int num = transform.childCount;
        for(int i = 0; i < num; i++)
        {
            transform.GetChild(i).gameObject.GetComponent<Rigidbody>().isKinematic = false;
            StartCoroutine(DestroyWallAfter());
        }
    }

    IEnumerator DestroyWallAfter()
    {
        yield return new WaitForSeconds(1.5f);
        Destroy(gameObject);
    }
}
