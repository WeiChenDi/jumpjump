﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public enum StageType
{
    NormalStage = 0,
    ObstacleStage = 1,
    MovableStage = 2,
    SwingStage = 3,
    CoinStage = 4,
}

public class Stage : MonoBehaviour
{

    public StageType stageType = StageType.NormalStage;
    public Vector3 direction = new Vector3(1, 0, 0);

    private const string path = "Prefabs/BrickWall";

    public bool isNewStage = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateObstacle()
    {
        GameObject obstacle = Resources.Load(path) as GameObject;
        obstacle = Instantiate(obstacle);
        obstacle.transform.localPosition += new Vector3(transform.position.x - direction.x, 0, transform.position.z - direction.z);
        if(direction.z == 1)
        {
            obstacle.transform.Rotate(new Vector3(0, 90, 0));
        }
    }
}
