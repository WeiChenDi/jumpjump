﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DataController : MonoBehaviour
{
    public GameData gameData;

    private string gameDataProjectFilePath = "/StreamingAssets/data.json";

    private LeaderBoardScript leaderBoard;

    private string dataFileName = "data.json";

    // Start is called before the first frame update
    void Start()
    {
        LoadLeaderBoard();
        //DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int GetTopScore()
    {
        return leaderBoard.topScore;
    }

    public void SubmitNewScore(int newScore)
    {
        if (newScore > leaderBoard.topScore)
        {
            leaderBoard.topScore = newScore;
            gameData.topScore = newScore;

            SaveLeaderBoard();
        }
    }

    private void LoadLeaderBoard()
    {
        leaderBoard = new LeaderBoardScript();
        LoadGameData();
        //if (PlayerPrefs.HasKey("topScore"))
        //{
        //    leaderBoard.topScore = PlayerPrefs.GetInt("topScore");
        //}
    }

    private void SaveLeaderBoard()
    {
        //PlayerPrefs.SetInt("topScore", leaderBoard.topScore);

        string dataAsJson = JsonUtility.ToJson(gameData);
        string filePath = Application.dataPath + gameDataProjectFilePath;
        File.WriteAllText(filePath, dataAsJson);
    }

    private void LoadGameData()
    {
        gameData = new GameData();
        string filePath = Path.Combine(Application.streamingAssetsPath, dataFileName);

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            GameData loadedData = JsonUtility.FromJson<GameData>(dataAsJson);
            leaderBoard.topScore = loadedData.topScore;
        }
        else
        {

        }
    }
}
