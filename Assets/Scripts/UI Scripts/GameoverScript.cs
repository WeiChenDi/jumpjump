﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;


public class GameoverScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowGameOverScreen()
    {
        transform.DOLocalMoveY(0.0f, 0.5f);
    }

    public void OnRestartButtClicked()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnMainMenuButtClicked()
    {
        SceneManager.LoadScene("MainMenu");
    }


}
