﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LeaderBoardScript : MonoBehaviour
{
    DataController dataController;

    public int currentScore = 0;
    public int topScore;

    public Text scoreText;
    public Text topScoreText;

    // Start is called before the first frame update
    void Start()
    {
        dataController = FindObjectOfType<DataController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnGameOver()
    {
        // dataController.SubmitNewScore(currentScore);
        ShowCurrentScore();
        this.GetComponent<GameoverScript>().ShowGameOverScreen();
        dataController.SubmitNewScore(currentScore);
    }

    void ShowCurrentScore()
    {
        scoreText.text = currentScore.ToString();
        topScore = dataController.GetTopScore();
        topScoreText.text = topScore.ToString();
    }
}
