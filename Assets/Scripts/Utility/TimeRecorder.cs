﻿using UnityEngine;
using System.Collections.Generic;

public class TimeRecorder
{
    public struct RecorderInfo
    {
        public float startTime;
        public float recordedTime;
        public bool isRecording;
    }

    static Dictionary<string, RecorderInfo> _recorders = new Dictionary<string, RecorderInfo>();

    public static void StartRecorder(string key)
    {
        RecorderInfo recorderInfo;
        if (!_recorders.TryGetValue(key, out recorderInfo))
        {
            recorderInfo = new RecorderInfo();
        }
        recorderInfo.startTime = Time.realtimeSinceStartup;
        recorderInfo.isRecording = true;
        _recorders[key] = recorderInfo;
    }

    public static void EndRecorder(string key)
    {
        RecorderInfo recorderInfo;
        if (_recorders.TryGetValue(key, out recorderInfo))
        {
            if (recorderInfo.isRecording)
            {
                recorderInfo.recordedTime += Time.realtimeSinceStartup - recorderInfo.startTime;
                recorderInfo.isRecording = false;
                _recorders[key] = recorderInfo;
            }
            else
            {
                Debug.LogWarning("Can't end record, because didn't start record.");
            }
        }
    }

    public static void Log(string key, string tip)
    {
        RecorderInfo recorderInfo;
        if (_recorders.TryGetValue(key, out recorderInfo) && !recorderInfo.isRecording)
        {
            Debug.LogError(tip + recorderInfo.recordedTime);
        }
        else
        {
            Debug.LogWarning("Not record or record not end.");
        }
    }

    public static void LogAll()
    {
        Debug.LogError("log all start ------------");
        foreach (var item in _recorders)
        {
            Debug.LogError(item.Key + "  " + item.Value.recordedTime);
        }
        Debug.LogError("log all end ------------");
    }

    public static void Clear()
    {
        _recorders.Clear();
    }
}
