﻿using System;
using System.Collections.Generic;

namespace CommonAPI
{
    public static class EventMgr
    {
        public delegate void Callback();
        public delegate void Callback<T>(T arg1);
        public delegate void Callback<T, U>(T arg1, U arg2);
        public delegate void Callback<T, U, V>(T arg1, U arg2, V arg3);
        public delegate void Callback<T1, T2, T3, T4>(T1 arg1, T2 arg2, T3 arg3, T4 arg4);

        public class EventReciver
        {
            public System.Delegate listener;
        }

        static Dictionary<EventTypeDef, List<EventReciver>> m_dicEventRcv = new Dictionary<EventTypeDef, List<EventReciver>>();

        public static void AddListener(EventTypeDef eventTypeID, Callback listener)
        {
            AddEvent(eventTypeID, listener);
        }
        public static void AddListener<T>(EventTypeDef eventTypeID, Callback<T> listener)
        {
            AddEvent(eventTypeID, listener);
        }
        public static void AddListener<T, N>(EventTypeDef eventTypeID, Callback<T, N> listener)
        {
            AddEvent(eventTypeID, listener);
        }

        public static void AddListener<T, U, V>(EventTypeDef eventTypeID, Callback<T, U, V> listener)
        {
            AddEvent(eventTypeID, listener);
        }

        internal static void AddListener(object onSwitchPage)
        {
            throw new NotImplementedException();
        }

        public static void AddListener<T1, T2, T3, T4>(EventTypeDef eventTypeID, Callback<T1, T2, T3, T4> listener)
        {
            AddEvent(eventTypeID, listener);
        }


        public static void Dispatch(EventTypeDef eventTypeID)
        {
            List<EventReciver> lstEventRcv = GetEventReciver(eventTypeID);
            if (lstEventRcv != null)
            {
                List<EventReciver> lstTmp = new List<EventReciver>(lstEventRcv);
                foreach (EventReciver eventRcv in lstTmp)
                {
                    try
                    {
                        if (!CheckInList(eventRcv, lstEventRcv))
                        {
                            continue;
                        }

                        Callback callback = (Callback)eventRcv.listener;
                        if (callback != null)
                        {
                            callback();
                        }
                    }
                    catch (Exception e)
                    {
                        UnityEngine.Debug.LogError(e);
                    }
                }
            }
        }

        public static void Dispatch<T>(EventTypeDef eventTypeID, T param1)
        {
            List<EventReciver> lstEventRcv = GetEventReciver(eventTypeID);

            if (lstEventRcv != null)
            {
                List<EventReciver> lstTmp = new List<EventReciver>(lstEventRcv);
                foreach (EventReciver eventRcv in lstTmp)
                {
                    try
                    {
                        if (!CheckInList(eventRcv, lstEventRcv))
                        {
                            continue;
                        }

                        Callback<T> callback = (Callback<T>)eventRcv.listener;
                        if (callback != null)
                        {
                            callback(param1);
                        }
                    }
                    catch (Exception e)
                    {
                        UnityEngine.Debug.LogError(e);
                    }
                }
            }
        }

        public static void Dispatch<T, N>(EventTypeDef eventTypeID, T param1, N param2)
        {
            List<EventReciver> lstEventRcv = GetEventReciver(eventTypeID);
            if (lstEventRcv != null)
            {
                List<EventReciver> lstTmp = new List<EventReciver>(lstEventRcv);
                foreach (EventReciver eventRcv in lstTmp)
                {
                    try
                    {
                        if (!CheckInList(eventRcv, lstEventRcv))
                        {
                            continue;
                        }

                        Callback<T, N> callback = (Callback<T, N>)eventRcv.listener;
                        if (callback != null)
                        {
                            callback(param1, param2);
                        }
                    }
                    catch (Exception e)
                    {
                        UnityEngine.Debug.LogError(e);
                    }
                }
            }
        }

        public static void Dispatch<T, U, V>(EventTypeDef eventTypeID, T param1, U param2, V param3)
        {
            List<EventReciver> lstEventRcv = GetEventReciver(eventTypeID);
            if (lstEventRcv != null)
            {
                List<EventReciver> lstTmp = new List<EventReciver>(lstEventRcv);
                foreach (EventReciver eventRcv in lstTmp)
                {
                    try
                    {
                        if (!CheckInList(eventRcv, lstEventRcv))
                        {
                            continue;
                        }

                        Callback<T, U, V> callback = (Callback<T, U, V>)eventRcv.listener;
                        if (callback != null)
                        {
                            callback(param1, param2, param3);
                        }
                    }
                    catch (Exception e)
                    {
                        UnityEngine.Debug.LogError(e);
                    }
                }
            }
        }

        public static void Dispatch<T1, T2, T3, T4>(EventTypeDef eventTypeID, T1 param1, T2 param2, T3 param3, T4 param4)
        {
            List<EventReciver> lstEventRcv = GetEventReciver(eventTypeID);
            if (lstEventRcv != null)
            {
                List<EventReciver> lstTmp = new List<EventReciver>(lstEventRcv);
                foreach (EventReciver eventRcv in lstTmp)
                {
                    try
                    {
                        if (!CheckInList(eventRcv, lstEventRcv))
                        {
                            continue;
                        }

                        Callback<T1, T2, T3, T4> callback = (Callback<T1, T2, T3, T4>)eventRcv.listener;
                        if (callback != null)
                        {
                            callback(param1, param2, param3, param4);
                        }
                    }
                    catch (Exception e)
                    {
                        UnityEngine.Debug.LogError(e);
                    }
                }
            }
        }

        public static void RemoveListener(EventTypeDef eventTypeID, Callback listner)
        {
            RemoveEvent(eventTypeID, listner);
        }
        public static void RemoveListener<T>(EventTypeDef eventTypeID, Callback<T> listner)
        {
            RemoveEvent(eventTypeID, listner);
        }

        public static void RemoveListener<T, N>(EventTypeDef eventTypeID, Callback<T, N> listner)
        {
            RemoveEvent(eventTypeID, listner);
        }

        public static void RemoveListener<T, U, V>(EventTypeDef eventTypeID, Callback<T, U, V> listner)
        {
            RemoveEvent(eventTypeID, listner);
        }
        public static void RemoveListener<T1, T2, T3, T4>(EventTypeDef eventTypeID, Callback<T1, T2, T3, T4> listner)
        {
            RemoveEvent(eventTypeID, listner);
        }

        public static void RemoveListener(EventTypeDef eventTypeID)
        {
            if (m_dicEventRcv.ContainsKey(eventTypeID))
            {
                m_dicEventRcv.Remove(eventTypeID);
            }
        }

        public static void RemoveAll()
        {
            m_dicEventRcv.Clear();
        }


        #region 私有方法

        private static bool CheckInList(EventReciver rcver, List<EventReciver> lstRcver)
        {
            if (lstRcver == null)
                return false;
            for (int i = 0; i < lstRcver.Count; i++)
            {
                if (lstRcver[i] == rcver)
                    return true;
            }
            return false;
        }

        private static void AddEvent(EventTypeDef eventTypeID, System.Delegate listener)
        {
            if (listener == null)
            {
                UnityEngine.Debug.LogError("AddEvent Fail! You have to set the value of  event response function!");
                return;
            }

            EventReciver eventRcv = new EventReciver();
            eventRcv.listener = listener;

            if (m_dicEventRcv.ContainsKey(eventTypeID))
            {
                List<EventReciver> lstEventRcv = m_dicEventRcv[eventTypeID];
                if (CheckDuplicate(lstEventRcv, eventRcv))
                    return;
                lstEventRcv.Add(eventRcv);
            }
            else
            {
                List<EventReciver> lstEventRcv = new List<EventReciver>();
                lstEventRcv.Add(eventRcv);
                m_dicEventRcv.Add(eventTypeID, lstEventRcv);
            }
        }

        private static bool CheckDuplicate(List<EventReciver> lstRcv, EventReciver checkedRcv)
        {
            foreach (EventReciver eventRcv in lstRcv)
            {
                if (eventRcv.listener == checkedRcv.listener)
                {
                    return true;
                }
            }
            return false;
        }

        private static List<EventReciver> GetEventReciver(EventTypeDef eventTypeID)
        {
            if (eventTypeID == EventTypeDef.None) return null;
            List<EventReciver> lstListeners = null;
            m_dicEventRcv.TryGetValue(eventTypeID, out lstListeners);
            return lstListeners;
        }

        private static void RemoveEvent(EventTypeDef eventTypeID, System.Delegate listener)
        {
            if (m_dicEventRcv.ContainsKey(eventTypeID))
            {
                List<EventReciver> lstEventRcv = m_dicEventRcv[eventTypeID];
                foreach (EventReciver eventRcv in lstEventRcv)
                {
                    if (eventRcv.listener == listener)
                    {
                        lstEventRcv.Remove(eventRcv);
                        break;
                    }
                }

                if (lstEventRcv.Count == 0)
                {
                    m_dicEventRcv.Remove(eventTypeID);
                }
            }
        }

        #endregion
    }
}


