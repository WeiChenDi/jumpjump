﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 通用类
/// </summary>
public class AppUtility
{
    /// <summary>
    /// 查找子节点
    /// </summary>
    public static Transform FindDeepChild(GameObject _target, string _childName, bool includeInactive = true)
    {
        Transform resultTrs = null;
        resultTrs = _target.transform.Find(_childName);
        if (resultTrs == null)
        {
            int count = _target.transform.childCount;
            for (int i = 0; i < count; i++)
            {
                Transform trs = _target.transform.GetChild(i);

                //忽略未激活的
                if (!includeInactive && !trs.gameObject.activeSelf)
                    continue;

                resultTrs = AppUtility.FindDeepChild(trs.gameObject, _childName);
                if (resultTrs != null)
                    return resultTrs;
            }
        }
        return resultTrs;
    }

    /// <summary>
    /// 查找子节点脚本
    /// </summary>
    public static T FindDeepChildComp<T>(GameObject _target, string _childName, bool includeInactive = true) where T : Component
    {
        Transform resultTrs = AppUtility.FindDeepChild(_target, _childName);
        if (resultTrs != null)
            return resultTrs.gameObject.GetComponent<T>();
        return (T)((object)null);
    }

    /// <summary>
    /// 查找子节点脚本
    /// </summary>
    public static T FindDeepChildCompSys<T>(GameObject _target, bool includeInactive = true) where T : Component
    {
        if (_target == null)
            return null;
        return _target.transform.GetComponentInChildren<T>(includeInactive);
    }

    /// <summary>
    /// 寻找父节点的组件(包含自己)
    /// </summary>
    public static T GetComponentInParent<T>(GameObject obj) where T : Component
    {
        T compl = default(T);
        compl = obj.GetComponent<T>();
        if (compl == null)
        {
            Transform parent = obj.transform.parent;
            if (parent != null)
            {
                compl = GetComponentInParent<T>(parent.gameObject);
            }
        }
        return compl;
    }

    /// <summary>
    /// 在所有父节点中找到指定组件（包含自己）
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="target"></param>
    /// <returns></returns>
    public static T GetComponentInParentDeep<T>(Transform target) where T : Component
    {
        if (target == null)
            return null;

        T result = target.GetComponent<T>();
        if (result == null)
        {
            Transform parent = target.parent;
            while (parent != null && result == null)
            {
                result = parent.GetComponent<T>();
                parent = parent.parent;
            }
        }

        return result;
    }

    /// <summary>
    /// 在所有父节点中找到指定所有组件（包含自己）
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="target"></param>
    /// <returns></returns>
    public static T[] GetComponentsInParentDeep<T>(Transform target) where T : Component
    {
        if (target == null)
            return null;

        T[] result = target.GetComponents<T>();
        Transform parent = target.parent;
        while (parent != null)
        {
            T[] tempResult = parent.GetComponents<T>();
            if (tempResult.Length > 0)
            {
                int length = result.Length;
                Array.Resize<T>(ref result, result.Length + tempResult.Length);
                tempResult.CopyTo(result, length);
            }
            parent = parent.parent;
        }

        return result;
    }

}
