﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering.PostProcessing;


public class ModeOneController : PlayerController
{

    private Text _volumeText;
    private int _sumVol = 0;
    private int _avgVol = 0;
    private int _cntVol = 0;
    private bool _canJump = false;
    private bool _jumped = false;
    private int _index = 0;
    private float _stretchingBtmVal = 20f;
    private const float _maxForce = 100f;
    //当前平台
    private delegate void StartCompressing();
    private delegate void StartStretching();
    private StartCompressing CompressingEvent;
    private StartStretching StretchingEvent;
    private PostProcessVolume _postProcessVolumn;
    private Vignette _vigLayer = null;
    private const float _startTime = 0.3f;
    private const float _stopTIme = 0.1f;


    public float factor;
    // Start is called before the first frame update
    void Start()
    {
        _rigid = GetComponent<Rigidbody>();
        _rigid.centerOfMass = new Vector3(0, 0f, 0);
        _body = GameObject.Find("Player").transform.Find("Body").gameObject;
        _head = GameObject.Find("Player").transform.Find("Head").gameObject;
        gameObject.AddComponent<MicrophoneTest>();
        _micro = GetComponent<MicrophoneTest>();
        GameObject canvas = GameObject.Find("Canvas");
        _volumeText = AppUtility.FindDeepChild(canvas, "VolumeValue").GetComponent<Text>();
        CompressingEvent += ModeOnePlayerShapeController.Instance.Compressing;
        StretchingEvent += ModeOnePlayerShapeController.Instance.Stretching;
        _nextStageComponent = SpawnStage.Instance.CreateNewStage();
        _postProcessVolumn = GameObject.Find("Main Camera").GetComponent<PostProcessVolume>();
        _postProcessVolumn.profile.TryGetSettings(out _vigLayer);
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckJump();
    }

    //如果有别的物体和本物体发生变化，会触发这函数
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Stage"))
        {
            SpawnStage.Instance.LastCollisionCollider = SpawnStage.Instance.CurrentStage.GetComponent<Collider>();
            SpawnStage.Instance.CurrentStage = collision.gameObject;
            if (SpawnStage.Instance.LastCollisionCollider != SpawnStage.Instance.CurrentStage.GetComponent<Collider>())
            {
                SpawnStage.Instance.RandomDirection();
                _nextStageComponent = SpawnStage.Instance.CreateNewStage();
                CameraController.Instance.MovingCamera();
            }

            Stage currentStage = collision.gameObject.GetComponent<Stage>();
            if (currentStage.isNewStage)
            {
                Transform currentStageCenter = currentStage.transform.GetChild(0);
                CenterMarkMgr center = currentStageCenter.GetComponent<CenterMarkMgr>();
                if(!center.collided)
                {
                    OnGetScore(currentStage);
                }
                else
                {
                    OnGetDoubleScore(currentStage);
                }
                if (currentStage.stageType == StageType.SwingStage)
                {
                    StopSwing(currentStage);
                }
            }

        }
        if (collision.gameObject.name == "Plane")
        {
            OnGameOver();
        }
        CompressingEvent();
    }



    private void OnJump()
    {
        if (_jumpForce > _stretchingBtmVal)
        {
            StretchingEvent();
        }
        Vector3 force = (new Vector3(0f, 1.0f, 0f) + _nextStageComponent.direction) * _jumpForce * factor;
        _rigid.AddForce(force, ForceMode.Impulse);
        StartCoroutine(ModeOnePostProcessStart(_jumpForce / _maxForce));
    }

    IEnumerator ModeOnePostProcessStart(float strength)
    {
        strength = Mathf.Min(strength, 0.5f);
        float deltaTime = 0.02f;
        float cnt = _startTime / deltaTime;
        float deltaIndensity = strength / cnt;
        float curTime = 0f;

        while (true)
        {
            if (curTime > _startTime)
            {
                StartCoroutine(ModeOnePostProcessStop());
                yield break;
            }
            curTime += deltaTime;
            _vigLayer.intensity.value += deltaIndensity;
            Mathf.Clamp(_vigLayer.intensity.value, 0f, 0.5f);
            yield return new WaitForSeconds(deltaTime);
        }
    }

    IEnumerator ModeOnePostProcessStop()
    {
        float deltaTime = 0.02f;
        float curTime = 0f;
        float cnt = _stopTIme / deltaTime;
        float startIndensity = _vigLayer.intensity.value;
        float _deltaIndensity = startIndensity / cnt;
        while (true)
        {
            if(curTime > _stopTIme)
            {
                yield break;
            }
            curTime += deltaTime;
            _vigLayer.intensity.value -= _deltaIndensity;
            Mathf.Clamp(_vigLayer.intensity.value, 0f, 0.7f);
            yield return new WaitForSeconds(deltaTime);
        }
    }

    void CheckJump()
    {
        if (_micro.volume <= 10)
        {
            //当检测到发声结束后开始跳跃
            if (_avgVol > 10)
            {
                _canJump = true;
                _jumpForce = _avgVol;
            }
            VolSetZero();
        }
        else
        {
            _sumVol += (int)_micro.volume;
            _cntVol++;
            _avgVol = _sumVol / _cntVol;
            _canJump = false;
        }
        if (_canJump && !_jumped)
        {
            OnJump();
            _canJump = false;
            _jumped = true;
        }
        if (_rigid.velocity.magnitude != 0)
        {
            _jumped = true;
        }
        else
        {
            _jumped = false;
        }
        _volumeText.text = _avgVol.ToString();
    }

    void VolSetZero()
    {
        _sumVol = 0;
        _avgVol = 0;
        _cntVol = 0;
    }
}
