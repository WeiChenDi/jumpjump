﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeOnePlayerShapeController : MonoBehaviour
{
    public float duration = 2f;
    private GameObject _body;
    private GameObject _head;
    private bool isCompressed = false;
    private static ModeOnePlayerShapeController _instance;
    public static ModeOnePlayerShapeController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.Find("Player").GetComponent<ModeOnePlayerShapeController>();
            }
            return _instance;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        _body = GameObject.Find("Player").transform.Find("Body").gameObject;
        _head = GameObject.Find("Player").transform.Find("Head").gameObject;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Compressing()
    {
        if (!isCompressed)
        {
            isCompressed = true;
            for (float cnt = 0; cnt < duration; cnt += Time.deltaTime)
            {
                _body.transform.localScale += new Vector3(1, -1, 1) * 0.01f * Time.deltaTime;
                _head.transform.localPosition += new Vector3(0, -1, 0) * 0.02f * Time.deltaTime;
            }
        }
    }

    public void Stretching()
    {
        if (isCompressed)
        {
            isCompressed = false;
            for (float cnt = 0; cnt < duration; cnt += Time.deltaTime)
            {
                _body.transform.localScale += new Vector3(-1, 1, -1) * 0.01f * Time.deltaTime;
                _head.transform.localPosition += new Vector3(0, 1, 0) * 0.02f * Time.deltaTime;
            }
        }
    }
}
