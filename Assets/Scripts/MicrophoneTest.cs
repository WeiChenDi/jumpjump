﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MicrophoneTest : MonoBehaviour
{
    public AudioClip micRecord;
    public string device;
    public float volume;
    public int rate = 16000;
    public int chunk = 512;
    private bool _isOver = false;
    public bool IsOver
    {
        get
        {
            return _isOver;
        }
        set
        {
            _isOver = value;
        }
    }

    private static MicrophoneTest _instance;
    public static MicrophoneTest Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.Find("Player").GetComponent<MicrophoneTest>();
            }
            return _instance;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        device = Microphone.devices[0];
        micRecord = Microphone.Start(device, true, 999, rate);
    }

    // Update is called once per frame
    void Update()
    {
        if(!_isOver)
        {
            GetVolume();
        }
    }

    public void GetVolume()
    {
        if(Microphone.IsRecording(null))
        {
            int sampleSize = 128;
            float[] samples = new float[sampleSize];
            int startPosition = Microphone.GetPosition(device) - (sampleSize + 1);
            micRecord.GetData(samples, startPosition);
            float levelMax = 0;
            for(int i = 0; i < sampleSize; ++i)
            {
                float wavePeak = samples[i];
                if(levelMax < wavePeak)
                {
                    levelMax = wavePeak;
                }
            }
            volume = levelMax * 99;
        }
        else
        {
            volume = 0;
        }
    }
}
