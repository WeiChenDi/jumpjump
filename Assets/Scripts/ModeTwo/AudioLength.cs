﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioLength : MonoBehaviour
{
    private int _w1;

    private bool _lastSil = true;
    private bool _sil = true;
    private float[] _es;
    private float _timeUsed = 0;
    private bool _recordDone = false;

    private float _audioLength = 0;
    private bool _isTalking = false;
    public bool IsTalking
    {
        get
        {
            return _isTalking;
        }
    }

    private delegate void StartGetAudio();
    private delegate void StopGetAidio();
    private StartGetAudio StartGettingEvent;
    private StopGetAidio StopGettingEvent;

    private ModeTwoController _modeTwoController;

    private static AudioLength _instance;
    public static AudioLength Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.Find("Player").GetComponent<AudioLength>();
            }
            return _instance;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _w1 = (int)(0.1 * MicrophoneTest.Instance.rate / MicrophoneTest.Instance.chunk) + 1;
        _es = new float[_w1];
        _modeTwoController = GameObject.Find("Player").GetComponent<ModeTwoController>();
        StartGettingEvent += _modeTwoController.CompressingToJump;
        StopGettingEvent += _modeTwoController.Jump;
    }

    // Update is called once per frame
    void Update()
    {
        GetAudio();
        RecordAudioLength();
    }

    public float GetAudioLength()
    {
        return _audioLength;
    }

    private void RecordAudioLength()
    {
        if(_isTalking)
        {
            _audioLength += Time.deltaTime;
        }
        else
        {
            _audioLength = 0;
        }
    }

    private void GetAudio()
    {
        float spk_e = 1e-3f;
        float sil_e = 1e-5f;
        float[] data = new float[MicrophoneTest.Instance.chunk];
        int offset = Microphone.GetPosition(MicrophoneTest.Instance.device) - MicrophoneTest.Instance.chunk + 1;
        if (offset < 0)
        {
            return;
        }
        _lastSil = _sil;
        MicrophoneTest.Instance.micRecord.GetData(data, offset);
        _sil = GetAudioLengthFunc(data, ref sil_e, ref spk_e);
        if (_lastSil && !_sil)
        {
            _isTalking = true;
            StartGettingEvent();
        }
        else if (_sil && !_lastSil)
        {
            _isTalking = false;
            StopGettingEvent();
        }
        return;
    }

    private bool GetAudioLengthFunc(float[] data, ref float sil_e, ref float spk_e)
    {
        float th_sil2spk = 0.5f;
        float th_spk2sil = 0.25f;
        float alpha = 0.975f;
        if (_es == null)
        {
            _es = new float[2];
        }
        if (Mathf.Max(data) > 1)
        {
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = data[i] / 32768.0f;
            }
        }
        float E = Var(data);
        _es[_es.Length - 1] = E;
        float th_eng_sil2spk = Mathf.Exp(Mathf.Log(spk_e) * th_sil2spk + Mathf.Log(sil_e) * (1 - th_sil2spk));
        float th_eng_spk2sil = Mathf.Exp(Mathf.Log(spk_e) * th_spk2sil + Mathf.Log(sil_e) * (1 - th_spk2sil));
        bool low = true;
        for (int i = 0; i < _es.Length; i++)
        {
            if (_es[i] >= th_eng_spk2sil)
            {
                low = false;
            }
        }
        if (low)
        {
            _sil = true;
        }
        else if (Mean(_es) > th_eng_sil2spk)
        {
            _sil = false;
        }
        if (E < th_eng_sil2spk && _sil)
        {
            if (E > sil_e)
            {
                sil_e = Mathf.Exp(Mathf.Log(sil_e) * alpha + Mathf.Log(E) * (1 - alpha));
            }
            else
            {
                sil_e = sil_e * alpha + E * (1 - alpha);
            }
        }
        else if (E > th_eng_spk2sil && !_sil)
        {
            if (E > spk_e)
            {
                spk_e = Mathf.Exp(Mathf.Log(spk_e) * alpha + Mathf.Log(E) * (1 - alpha));
            }
            else
            {
                spk_e = spk_e * alpha + E * (1 - alpha);
            }
        }
        for (int i = 0; i < _es.Length - 1; i++)
        {
            _es[i] = _es[i + 1];
        }
        _es[_es.Length - 1] = 0;
        return _sil;
    }

    private float Var(float[] data)
    {
        float avg = Mean(data);
        float varSum = 0;
        foreach (var d in data)
        {
            varSum += Mathf.Pow(d - avg, 2);
        }
        return varSum / data.Length;
    }

    private float Mean(float[] data)
    {
        float sum = 0;
        foreach (var d in data)
        {
            sum += d;
        }
        return sum / data.Length;
    }
}
