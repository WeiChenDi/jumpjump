﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.Rendering.PostProcessing;

public class ModeTwoController : PlayerController
{
    private Text _volumeText;
    private float _audioTime;
    private bool _isCompressing = false;
    private int _compressingCnt = 0;
    private const int _maxCompressingCnt = 40;
    private PostProcessVolume _postProcessVolumn;
    private Vignette _vigLayer = null;
    private const float _maxForce = 100f;
    public float factor;

    // Start is called before the first frame update
    void Start()
    {
        _rigid = GetComponent<Rigidbody>();
        _rigid.centerOfMass = new Vector3(0, 0f, 0);
        _body = GameObject.Find("Player").transform.Find("Body").gameObject;
        _head = GameObject.Find("Player").transform.Find("Head").gameObject;
        gameObject.AddComponent<MicrophoneTest>();
        _micro = GetComponent<MicrophoneTest>();
        GameObject canvas = GameObject.Find("Canvas");
        _volumeText = AppUtility.FindDeepChild(canvas, "VolumeValue").GetComponent<Text>();
        _nextStageComponent = SpawnStage.Instance.CreateNewStage();
        _postProcessVolumn = GameObject.Find("Main Camera").GetComponent<PostProcessVolume>();
        _postProcessVolumn.profile.TryGetSettings(out _vigLayer);
    }

    // Update is called once per frame
    void Update()
    {
        if(_isCompressing && _compressingCnt < _maxCompressingCnt)
        {
            _body.transform.localScale += new Vector3(1, -1, 1) * 0.05f * Time.deltaTime;
            _head.transform.localPosition += new Vector3(0, -1, 0) * 0.1f * Time.deltaTime;

            SpawnStage.Instance.CurrentStage.transform.localScale += new Vector3(0, 0, -1) * 0.3f * Time.deltaTime;

            _compressingCnt++;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Stage"))
        {
            SpawnStage.Instance.LastCollisionCollider = SpawnStage.Instance.CurrentStage.GetComponent<Collider>();
            SpawnStage.Instance.CurrentStage = collision.gameObject;
            if(SpawnStage.Instance.LastCollisionCollider != SpawnStage.Instance.CurrentStage.GetComponent<Collider>())
            {
                SpawnStage.Instance.RandomDirection();
                _nextStageComponent = SpawnStage.Instance.CreateNewStage();
                CameraController.Instance.MovingCamera();
            }

            Stage currentStage = collision.gameObject.GetComponent<Stage>();
            if (currentStage.isNewStage)
            {
                Transform currentStageCenter = currentStage.transform.GetChild(0);
                CenterMarkMgr center = currentStageCenter.GetComponent<CenterMarkMgr>();
                if (!center.collided)
                {
                    OnGetScore(currentStage);
                }
                else
                {
                    OnGetDoubleScore(currentStage);
                }
            }
            if (currentStage.stageType == StageType.SwingStage)
            {
                StopSwing(currentStage);
            }
        }
        if (collision.gameObject.name == "Plane")
        {
            OnGameOver();
        }
    }

    public void CompressingToJump()
    {
        StartCoroutine(ModeTwoPostProcessStart());
        _isCompressing = true;
    }

    public void Jump()
    {
        _isCompressing = false;
        _compressingCnt = 0;
        _jumpForce = (int)(GameObject.Find("Player").GetComponent<AudioLength>().GetAudioLength() * 100);
        Stretching();
        OnJump();
    }

    private void OnJump()
    {
        _volumeText.text = _jumpForce.ToString();
        Vector3 force = (new Vector3(0f, 1.0f, 0f) + _nextStageComponent.direction) * _jumpForce * factor;
        _rigid.AddForce(force, ForceMode.Impulse);
    }

    IEnumerator ModeTwoPostProcessStart()
    {
        float deltaTime = 0.02f;
        float deltaIndensity = 0.03f;
        while(true)
        {
            if(!AudioLength.Instance.IsTalking)
            {
                StartCoroutine(ModeTwoPostProcessStop());
                yield break;
            }
            _vigLayer.intensity.value += deltaIndensity;
            Mathf.Clamp(_vigLayer.intensity.value, 0f, 1f);
            yield return new WaitForSeconds(deltaTime);
        }
    }

    IEnumerator ModeTwoPostProcessStop()
    {
        float deltaTime = 0.02f;
        float currentTime = 0f;
        float deltaIndensity = 0.1f;
        while (true)
        {
            if(_vigLayer.intensity.value <= 0)
            {
                yield break;
            }
            currentTime += deltaTime;
            _vigLayer.intensity.value -= deltaIndensity;
            yield return new WaitForSeconds(deltaTime);
        }
    }

    public void Stretching()
    {
        _body.transform.DOScale(0.1f, 0.5f);
        _head.transform.DOLocalMoveY(0.29f, 0.5f);

        SpawnStage.Instance.CurrentStage.transform.DOScale(new Vector3(SpawnStage.Instance.CurrentStage.transform.localScale.x,SpawnStage.Instance.CurrentStage.transform.localScale.y, 1f), 0.2f);
    }
}
